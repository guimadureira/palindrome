#include <stdio.h>
#include <stdlib.h>
#include "estatistica.h"

double media(int n, double *v)
{
    double temp =0;
    for(int i=0;i<n;i++)
    {
        temp += v[i];
    }
    temp = temp/n;
    return temp;
}
