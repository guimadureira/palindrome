#include <stdio.h>
#include <stdlib.h>
#include "desvio.h"
#include "estatistica.h"
#include <math.h>

double desvio(int n, double *v)
{
    double soma = 0;
    double media = media(n, v);
    for(int i=0;i<n;i++)
    {
        double temp =0;
        temp = pow((v[i]-media), 2);
        soma += temp;
    }
    soma = sqrt(soma/(n-1));
    return soma;
}
